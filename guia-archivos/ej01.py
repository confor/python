'''
cosas usadas en este archivo, en orden cronológico:
https://docs.python.org/3/tutorial/datastructures.html#dictionaries
https://docs.python.org/3/library/json.html#module-json
https://docs.python.org/3/tutorial/inputoutput.html
https://stackoverflow.com/a/2258273
https://www.programiz.com/python-programming/methods/built-in/sorted
https://docs.python.org/3/library/json.html#json.JSONDecodeError
https://docs.python.org/3/tutorial/errors.html#handling-exceptions
http://book.pythontips.com/en/latest/map_filter.html
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
https://stackoverflow.com/questions/31127824/what-are-pythons-equivalent-of-javascripts-reduce-map-and-filter
http://book.pythontips.com/en/latest/map_filter.html
https://stackoverflow.com/questions/4081217/how-to-modify-list-entries-during-for-loop

'''


import json

ID = 'numero de cuenta'
NAME = 'nombre de cliente'
HOME = 'domicilio'
ADDR = 'calle y numero'
CITY = 'ciudad'
PHONE = 'telefono'
CASH = 'saldo'

USUARIO = {
    ID: 1001,
    NAME: "asdfasdf",
    HOME: {
        ADDR: "sadsasfd",
        CITY: "sadfsd",
        PHONE: "2948723"
    },
    CASH: 420.69
}

DATA = []


def read():
    global DATA

    try:
        file = open('ej01.json', 'r')
        DATA = json.load(file)
        file.close()
    except FileNotFoundError:
        print("el archivo que debería contener json no existe")
        return False
    except json.JSONDecodeError as err:
        print("json inválido", err)
        return False

    return True

def save():
    global DATA

    try:
        trash = json.dumps(DATA, indent=2)
        file = open('ej01.json', 'w')
        if file.write(trash) is None:
            print('no se pudo escribir json al archivo')

    except FileNotFoundError:
        print('el archivo a escribir no existe')

    return


def find_last_id():
    last_acc = sorted(DATA, key=lambda acc: acc[ID], reverse=True)[0]
    return last_acc[ID]


# utilidad para pedir un número al usuario
def ask_int(msg):
    print(msg, end=': ')
    try:
        return int(input())

    # atrapar no-números
    except ValueError:
        print('\nSe esperaba un número :(')
        return ask_int(msg)

    # atrapar ^C y salir
    except KeyboardInterrupt:
        raise SystemExit


def new_client():
    print('\n\n')
    new = {}
    new[ID] = find_last_id() + 1
    new[HOME] = {}

    print('ingrese', NAME)
    new[NAME] = input()

    print('acerca del domicilio: ingrese', ADDR)
    new[HOME][ADDR] = input()

    print('acerca del domicilio: ingrese', CITY)
    new[HOME][CITY] = input()

    print('teléfono?')
    new[HOME][PHONE] = input()

    new[CASH] = 0

    DATA.append(new)
    save()
    print('cliente guardado. su cuenta nueva es', new[ID])

    return


def deposit():
    id = ask_int('depositar en que cuenta?')

    accounts = list(filter(lambda acc: acc[ID] == id, DATA))

    if len(accounts) != 1:
        print('esa cuenta no esta en nuestro sistema')
        return

    amount = ask_int('cuanta plata meter al banco?')

    if amount <= 0:
        print('que cagao')
        return

    # revisar cada cuenta y cuando se encuentre la
    # deseada, meterle la plata
    for i, account in enumerate(DATA):
        if account[ID] != id:
            continue

        DATA[i][CASH] += amount
        print('añadidos', amount, 'pesos')
        break

    save()

    return


def withdraw():
    id = ask_int('que numero de cuenta?')

    accounts = list(filter(lambda acc: acc[ID] == id, DATA))

    if len(accounts) != 1:
        print('esa cuenta no existe')
        return

    acc = accounts[0]
    amount = ask_int('cuanta monei retirar?')
    if amount <= 0:
        print('en serio?')
        return

    elif acc[CASH] <= amount:
        print('esa cuenta no tiene tanta plata :(')
        return

    # modificar el arreglo que tiene todas las cuentas
    # no tengo ni idea si los bucles dan una variable que muta
    # el arreglo original, asi como el foreach de php cuando
    # se pasan por referencia
    for i, account in enumerate(DATA):
        if account[ID] != id:
            continue

        DATA[i][CASH] -= amount
        print('listo :D')
        break

    save()

    return


def info():
    id = ask_int('que num. de cuenta?')

    accounts = list(filter(lambda acc: acc[ID] == id, DATA))

    if len(accounts) != 1:
        print('esa cuenta no esta en nuestro sistema')
        return

    acc = accounts[0]

    print('\n')
    print(NAME + ':', acc[NAME])
    print(ID + ':', id)
    print(CASH + ':', acc[CASH])
    print(HOME + ':')
    for tag in [ADDR, CITY, PHONE]:
        print('-', tag + ':', acc[HOME][tag])
    print('\n')

    return


def main():
    if read() is not True:
        return print('algo salió mal x_x')

    print('sistema ultra profesional del banco de icb xd')
    print('que acción quiere realizar?\n')

    valid = False
    while not valid:
        print(' 1. agregar un cliente al banco')
        print(' 2. depositar en una cuenta')
        print(' 3. retirar de una cuenta')
        print(' 4. mostrar información tras una cuenta')
        print(' 5. salir')

        print(' > ', end='')

        choice = input()
        if choice == '1':
            new_client()

        elif choice == '2':
            deposit()

        elif choice == '3':
            withdraw()

        elif choice == '4':
            info()

        elif choice == '5':
            print('saliendo...')
            break

        else:
            print('try again bitch')
            valid = False
            continue


main()
