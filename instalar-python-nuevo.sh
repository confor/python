#!/bin/bash

echo Instalador de Python 3
echo .

VERSION=`lsb_release -d | awk -F"\t" '{print $2}'`
CORES=`awk '/^processor/{print $3}' /proc/cpuinfo | wc -l`
sanitization='^[0-9]+$'
# gracias https://stackoverflow.com/a/806923
if ! [[ $CORES =~ $sanitization ]] ; then
    CORES=1
fi

echo Distribución detectada: $VERSION
echo Núcleos detectados: $CORES
echo ''

echo Seleccione la versión de python a instalar
echo '1) 3.5.6'
echo '2) 3.7.1'
read option

case $option in
    1) PYTHON_VER="3.5.6" ;;
    2) PYTHON_VER="3.7.1" ;;
    *) echo "Opción inválida. Saliendo." && exit 1
esac

echo Preparando python $PYTHON_VER

TMP_DIR=/tmp/python3
DIR_INSTALL=$HOME/Repositorio/python-$PYTHON_VER

if [ -d "$DIR_INSTALL" ]; then
    echo Python ya instalado en $DIR_INSTALL
    echo Saliendo
    exit 1
else
    mkdir -p $DIR_INSTALL
fi

LOG_FILE=$DIR_INSTALL/installation.log
touch $LOG_FILE

echo Instalando dependencias
echo Posiblemente necesita contraseña de superusuario
DEPENDENCIES="libreadline-dev libncursesw5-dev libssl-dev zlib1g-dev libbz2-dev liblzma-dev tk8.6-dev tcl8.6-dev libcairo2-dev libgirepository1.0-dev libsqlite3-dev"

if [[ $VERSION == *"Ubuntu"* ]] ; then
    sudo apt-get update
    sudo apt-get install --no-install-recommends -y build-essential
    sudo apt-get install -y $DEPENDENCIES

elif [[ $VERSION == *"Debian"* ]] ; then
    su - -c 'apt-get update && apt-get install --no-install-recommends -y build-essential && apt-get install -y '$DEPENDENCIES

fi

mkdir -p $TMP_DIR
cd $TMP_DIR

echo Obteniendo código fuente
wget -O Python-$PYTHON_VER.tgz 'https://www.python.org/ftp/python/'$PYTHON_VER'/Python-'$PYTHON_VER'.tgz'
tar xf Python-$PYTHON_VER.tgz
cd Python-$PYTHON_VER

echo Configurando
./configure --prefix=$DIR_INSTALL --with-ensurepip >> $LOG_FILE

echo Compilando
make -j$CORES >> $LOG_FILE

echo Instalando
make install >> $LOG_FILE

echo Eliminando archivos temporales
cd $HOME
rm -r /tmp/python3

echo ''
echo ''
echo ok
echo Instalado en $DIR_INSTALL
