# Laberinto
Simple juego de escapar un laberinto, implementado en python 3.5.

### Obteniendo el juego
Clonar este repositorio y ejecutar `python game.py` dentro de la carpeta `laberinto`.

### Prerequisitos
Python 3. Desarrollado y probado en 3.5.6.

### Juego
Un laberinto es un recinto lleno de caminos parecidos y cortos entrecruzados, dispuestos de tal manera que dificulta el escape. En este proyecto las paredes son representadas por bloques sólidos, y el jugador por la letra `O`. La meta es escapar: salir por el lado opuesto otorgará la victoria.

### Controles
El jugador se controla con las flechas del teclado. La selección del menú se mueve con las flechas arriba y abajo, seleccionando con enter.

### Sobre el código
El proyecto está separado en cuatro archivos.

`input.py` ofrece una función que permite obtener las teclas presionadas en un emulador de terminal, con un tratamiento especial para los caracteres que no son una letra (ANSI control sequences, según Wikipedia), y también agrega nombres a las flechas del teclado, porque son lo que más se usa en el código.

`maze.py` implementa `maze.js` en una clase de python. La generación de laberinto funciona de la siguiente manera: primero, al crear una nueva instancia de Maze, se exige el tamaño en filas y columnas. La función `build` crea un arreglo unidimensional con tamaño `2y*(2x+2)+(2x+2)`, donde `x = columnas` e `y = filas`. Un bucle va desde el final hacia el inicio llenando cada espacio con un número que lo identifica como una pared, y por cada múltiplo de `2x+2` se identifica un espacio como un salto de línea, indicando el final de la fila actual y el comienzo de la siguiente fila. La función `F(p)` definida dentro de `build()` en `Maze` decide aleatoriamente por cada dirección si es que se vaciará o no. La constante por la que se multiplica `random()` decidirá el aspecto del laberinto: valores entre 0 y 1 entregan muchas líneas horizontales; entre -1 y 0 hay muchas líneas verticales; números menores a 4 o 5 generalmente entregan laberintos decentes, que se pueden resolver en un camino obvio sin muchas bifurcaciones, pero números grandes empiezan a hacer el laberinto más dificil: se ven caminos falsos mucho más largos que a veces rodean gran parte del tablero.

`utils.py` contiene sólo funciones de utilidad para imprimir texto y limpiar el buffer del emulador de terminal.

`game.py` es quien une la lógica de los anteriores tres, además de implementar menus básicos.

El proyecto entero fue desarrollo en conjunto con `flake8`, un [_linter_](https://en.wikipedia.org/wiki/Lint_(software)) para cuidar que uno sigue el `PEP8` al pie de la letra.

La generación del laberinto fue basada 100% en una [publicación de CodeGolf en StackExchange](https://codegolf.stackexchange.com/a/49642) donde la solución propuesta por `edc65` de 174 bytes (!!!) en Javascipt fue de-ofuscado por mi con mucha paciencia y con debugging en Chrome Developer Tools se entendió cómo funciona. Luego fue portado a Python. El código original se puede encontrar en el enlace, y mi versión deofuscada está en `maze.js`. Fue probado numerosas veces tanto en node 8.9.1 como en chromium 67, ambos con el motor V8 versión 6. Mi versión en javascript fue escrita con ECMAScript 6/2015 en mente, pero creo que es trivial convertirlo para que funcione incluso en Internet Explorer 6. La versión de python tiene algunos arreglos para lidiar con el cambio de lenguaje, pero es la misma lógica explicada unos parrafos más arriba.

El código presenta problemas conocidos, aunque ninguno es un impedimento para jugar:

- Las flechas del teclado se interpretan como el caracter ANSI que mueve el cursor en emuladores de terminal (`^[A`, `^[B`, `^[C`, `^[D`). Es posible que algunos emuladores de terminal no se lleven bien con esto. El juego sólo fue probado en `pantheon-terminal` y `urxvt`.
- Un laberinto no puede ser más grande que 70x50 o algo así. Python tira una excepción de que el _call stack_ es excedido. Dado el enorme tamaño de esos laberintos, no lo considero un problema muy grave.
- A veces la terminal se actualiza lento y se produce flicker, sobre todo cuando uno presiona un montón de teclas a la vez.
