const EMPTY = 0,
	  FULL = 1,
	  ROW_END = '\n';

function build(r,c) {
	var width = 2 * c + 2,
		total = 2 * r * width + width,
		maze = [];

	function F(p) {
		let arr = [width, 1, -width, -1],
			result = [];

		let k = 0,
			i = Math.random() * 8;

		while (k < arr.length) {
			k++;
			let s = p + 2 * arr[k+i&3];
			if (maze[s] > 0) {
				maze[(p+s)/2] = EMPTY;
				maze[s] = EMPTY;
				result[k] = F(s);
			} else {
				result[k] = false;
			}
		}

		return result;
	}

	while (total--) {
		if (total % width)
			maze[total] = FULL;
		else
			maze[total] = ROW_END;
	}

	F(width + 2);

	return maze;
}

var maze = build(8, 20),
	drawing = '';

for (let i = 0; i < maze.length; i++) {
	let tile = maze[i];

	if (tile === FULL)
		drawing += '█';
	else if (tile === EMPTY)
		drawing += ' ';
	else if (tile === ROW_END)
		drawing += '\n';
	else
		drawing += tile;
}

console.log(drawing);
