# -*- coding: utf-8 -*-

from random import random


class Maze:
    '''
    aquí debería ir documentación, pero mejor voy a explicar un problema con
    la función build.
    la variable `maze` es un diccionario, aunque podría ser un arreglo si es
    que se inicializa entera por comprensión. tiene que tener un largo de
    `2 * rows * (2*cols+2) + (2*cols+2)`, de lo contrario la función F(p) en
    el bucle tirará KeyError.
    con la clase de programación i del 26 de septiembre aprendimos listas por
    comprensión y expresión, es posible que este conocimiento permita corregir
    el código actual, pero la motivación es casi nula debido a que este ya
    funciona bien. no hay que reparar algo que no está roto ¯\\_(ツ)_/¯.
    '''

    EMPTY = 0
    FULL = 1
    ROW_END = 2
    START = 3
    FINISH = 4

    rows = 0
    cols = 0
    maze = {}

    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols

    def build(self):
        width = 2 * self.cols + 2
        total = 2 * self.rows * width + width
        maze = {}
        arr = [width, 1, -width, -1]

        def F(p):
            result = {}

            k = 0
            i = random() * 3.14

            # 4 = len(arr)
            while k < 4:
                k = k + 1
                s = p + 2 * arr[int(k+i) & 3]

                if s not in maze or maze[s] == self.ROW_END:
                    result[k] = False
                elif maze[s] > 0:
                    maze[(p+s)/2] = self.EMPTY
                    maze[s] = self.EMPTY
                    result[k] = F(s)
                else:
                    result[k] = False

            return result

        # aquí solía ser `while total:`, pero resulta que como la lista va
        # desde el final hacia el inicio, se crea un self.ROW_END extra
        # entonces con `total > 1` se evita ese ultimo chequeo
        while total > 1:
            total = total - 1
            if total % width:
                maze[total] = self.FULL
            else:
                maze[total] = self.ROW_END

        F(width + 2)

        self.maze = maze
        return self.maze
