#!/usr/bin/env python
# -*- coding: utf-8 -*-

from maze import Maze
from input import input, Key
from utils import raw_print, debug, raw_debug, clear_term

import sys
from collections import namedtuple, OrderedDict

Tile = namedtuple('Tile', ['type', 'sprite'])
Point = namedtuple('Point', ['x', 'y'])
MenuChoice = namedtuple('MenuChoice', ['message', 'action', 'key'])


class Board:
    cols = 8
    rows = 4
    width = 0
    height = 0
    maze = {}
    board = []

    WALL = '█'
    SPACE = ' '
    PLAYER = 'O'
    TRACE = 'o'
    UNKNOWN = '.'

    def __init__(self, cols, rows):
        self.cols = cols
        self.rows = rows

    def print_maze(self):
        maze = self.maze
        drawing = ''

        for k, tile in maze.items():
            if tile == Maze.FULL:
                drawing += Board.WALL
            elif tile == Maze.EMPTY:
                drawing += Board.SPACE
            elif tile == Maze.ROW_END:
                drawing = drawing + '\n'
            else:
                drawing = drawing + str(tile)

        print(drawing)
        return

    def tile(self, x, y):
        if x < 0 or y < 0:
            return False

        if x >= self.width:
            return False

        if y >= self.height:
            return False

        return self.board[y][x]

    def fill(self):
        # maze width es de 2col+2 pero podemos -1 por el
        # salto de linea que se elimina al final
        width = 2 * self.cols + 1
        height = 2 * self.rows + 3

        nothing = Tile(Maze.EMPTY, Board.SPACE)
        board = [[nothing for x in range(width)] for y in range(height)]

        maze = Maze(self.rows, self.cols).build()

        # entrada y salida
        maze[2] = Maze.EMPTY
        maze[len(maze)-1] = Maze.EMPTY

        curr_row = 1  # partir una linea más abajo
        curr_column = 0

        for k, tile in maze.items():
            if tile == Maze.FULL:
                board[curr_row][curr_column] = Tile(Maze.FULL, Board.WALL)
                curr_column += 1
            elif tile == Maze.EMPTY:
                board[curr_row][curr_column] = Tile(Maze.EMPTY, Board.SPACE)
                curr_column += 1
            elif tile == Maze.ROW_END:
                curr_row += 1
                curr_column = 0
            elif tile == Maze.START or tile == Maze.FINISH:
                board[curr_row][curr_column] = Tile(tile, Board.SPACE)
                curr_column += 1
            else:
                board[curr_row][curr_column] = Tile(tile, Board.UNKNOWN)
                curr_column += 1

        self.width = width
        self.height = height
        self.maze = maze
        self.board = board
        return board


def menu_quit():
    clear_term()
    sys.exit(0)


def main_menu():
    choices = OrderedDict()
    choices[0] = MenuChoice('Jugar: fácil', lambda: play(12, 6), 'a')
    choices[1] = MenuChoice('Jugar: difícil', lambda: play(24, 8), 'b')
    choices[2] = MenuChoice('Salir', menu_quit, 'q')

    # primer item de las opciones
    chosen = 0
    desired_action = lambda: None
    wants_action = False
    key = Key('\r', '')

    while True:
        clear_term()

        print('  _       _               _       _        ')
        print(' | |     | |             (_)     | |       ')
        print(' | | __ _| |__   ___ _ __ _ _ __ | |_ ___  ')
        print(' | |/ _` | \'_ \\ / _ \\ \'__| | \'_ \\| __/ _ \\ ')
        print(' | | (_| | |_) |  __/ |  | | | | | || (_) |')
        print(' |_|\\__,_|_.__/ \\___|_|  |_|_| |_|\\__\\___/ ')
        print('')
        print('--------------------------------------------\n')

        if key.name == 'arrow up':
            chosen -= 1

        elif key.name == 'arrow down':
            chosen += 1

        if chosen < 0:
            chosen = 0
        elif chosen > len(choices) - 1:
            chosen = len(choices) - 1

        for i, choice in choices.items():
            if choice.key == key.name:
                wants_action = True
                desired_action = choice.action

            if i == chosen:
                print(' -->', choice.message)

                if key.name == '\r':
                    wants_action = True
                    desired_action = choice.action

            else:
                print('    ', choice.message)

        print('\n')
        print('Mover con flechas del teclado. Aceptar con enter.')
        print('Salir con `q` en cualquier momento.')

        debug(key)
        debug('current choice:', chosen)

        if wants_action:
            wants_action = False
            desired_action()
            key = Key('', '')
        else:
            key = input()

    return


def play(cols, rows):
    instance = Board(cols, rows)
    board = instance.fill()
    playerpos = Point(1, 0)
    goal = Point(instance.width - 2, instance.height - 1)
    directions = {
        'arrow up': Point(0, -1),
        'arrow down': Point(0, 1),
        'arrow left': Point(-1, 0),
        'arrow right': Point(1, 0)
    }

    movements = 0
    movementsarr = []

    def draw_moves():
        deduped_moves = []
        for move in movementsarr:
            if move not in deduped_moves:
                deduped_moves.append(move)

        for y, row in enumerate(board):
            raw_debug('%3d ' % y)  # `%` deprecated?

            for x, tile in enumerate(row):
                type, sprite = tile

                move_in_this_tile = False

                for move in deduped_moves:
                    if x == move.x and y == move.y:
                        raw_print(Board.TRACE)
                        move_in_this_tile = True

                if move_in_this_tile is not True:
                    raw_print(sprite)

            raw_print('\n')

    def draw():
        for y, row in enumerate(board):
            raw_debug('%3d ' % y)  # `%` deprecated?

            for x, tile in enumerate(row):
                type, sprite = tile

                if x == playerpos.x and y == playerpos.y:
                    raw_print(Board.PLAYER)
                elif x == goal.x and y == goal.y:
                    raw_print(Board.SPACE)
                else:
                    raw_print(sprite)

            raw_print('\n')

    while True:
        clear_term()
        draw()
        debug('current pos:', playerpos)
        debug('current tile:', instance.tile(playerpos.x, playerpos.y))
        debug('press q to give up, r to reset')

        if goal.x == playerpos.x and goal.y == playerpos.y:
            clear_term()
            draw_moves()
            debug('player finished with', movements, 'moves')

            '''debug('these are the movements:')
            for move in movementsarr:
                debug('movimiento:', move)
            '''

            print('Terminó con', movements, 'movimientos')
            print('Felicidades!')
            print('Presione `q` para salir, o `r` para reiniciar.')

        key = input()
        px, py = playerpos

        if key.name in directions:
            extrax, extray = directions[key.name]
            newpos = Point(px + extrax, py + extray)
            check = instance.tile(newpos.x, newpos.y)

            debug('trying to move to', newpos)

            if check is not False and check.type != Maze.FULL:
                playerpos = newpos
                movements += 1
                movementsarr.append(playerpos)

        elif key.code == 'q':
            clear_term()
            break

        elif key.code == 'r':
            playerpos = Point(1, 0)
            movements = 0
            movementsarr = []

        else:
            debug('unknown key')


def main():
    main_menu()


main()
