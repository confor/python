import os


def raw_print(*args):
    return print(*args, end='')


def debug(*args):
    if 'DEBUG' in os.environ:
        return print(*args)
    else:
        return


def raw_debug(*args):
    if 'DEBUG' in os.environ:
        return raw_print(*args)
    else:
        return


def clear_term():
    raw_print('\033c')
