from collections import OrderedDict

ROMANOS = {
    1: 'I',
    2: 'II',
    3: 'III',
    4: 'IV',
    5: 'V',
    6: 'VI',
    7: 'VII',
    8: 'IIX',
    9: 'IX',
    10: 'X',
    50: 'L',
    100: 'C',
    500: 'D',
    1000: 'M'
}


def pedir_num():
    n = 0
    while n <= 0 or n > 4000:
        print("ingrese un entero entre 1 y 4000:")
        n = int(input())

    return n


def num_to_roman(num):
    ROMANOS = OrderedDict()
    ROMANOS['M'] = 1000
    ROMANOS['CM'] = 900
    ROMANOS['D'] = 500
    ROMANOS['CD'] = 400
    ROMANOS['C'] = 100
    ROMANOS['XC'] = 90
    ROMANOS['L'] = 50
    ROMANOS['XL'] = 40
    ROMANOS['X'] = 10
    ROMANOS['IX'] = 9
    ROMANOS['V'] = 5
    ROMANOS['IV'] = 4
    ROMANOS['I'] = 1

    romano = ""

    for letra, valor in ROMANOS.items():
        while num >= valor:
            transformado = letra
            num = num - valor

    return transformado


'''
def num_to_roman(num):
    if num in ROMANOS:
        return ROMANOS[num]

    lista = []
    num = str(num)

    for i in num:
        lista.append(int(i))

    for i, digito in enumerate(lista):
        print("el digito", i, "es", lista[i])

    print("el numero dado es", num, "y se transformo a", lista)

    return lista
'''

num = pedir_num()
print(num_to_roman(num))
