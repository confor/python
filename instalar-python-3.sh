#!/bin/bash
echo Seleccione la version de Python a instalar
echo 1 - 3.5.6
echo 2 - 3.7.0
read option

case $option in
    1) PYTHON_VER="3.5.6" ;;
    2) PYTHON_VER="3.7.0" ;;
    *) echo "Opción Inválida"
esac

echo Ha seleccionado $PYTHON_VER
TMP_DIR=/tmp/python3
DIR_INSTALL=$HOME/Repositorio/python-$PYTHON_VER

if [ -d "$DIR_INSTALL" ]; then
    echo python ya está instalado, escapando
    exit 1
else
    mkdir -p $DIR_INSTALL
fi

echo descargando el código fuente

mkdir -p $TMP_DIR
cd $TMP_DIR
wget -O Python-$PYTHON_VER.tgz 'https://www.python.org/ftp/python/'$PYTHON_VER'/Python-'$PYTHON_VER'.tgz'
tar xf Python-$PYTHON_VER.tgz
cd Python-$PYTHON_VER

echo Su version de S.O es
VERSION=`lsb_release -d | awk -F"\t" '{print $2}'`
echo $VERSION

echo instalando dependencias

if [[ $VERSION == *"Ubuntu"* ]] ; then

    echo probablemente necesita permisos sudo
    sudo apt-get update && sudo apt-get install build-essential libssl-dev zlib1g-dev libbz2-dev libsqlite3-dev

elif [[ $VERSION == *"Debian"* ]] ; then

    su - -c 'apt-get update && apt-get install build-essential libssl-dev zlib1g-dev libbz2-dev libsqlite3-dev -y'

fi

./configure --prefix=$DIR_INSTALL

#nucleos=`cat /proc/cpuinfo | grep cores | head -n 1 | cut -d" " -f 3`
nucleos=`awk '/^processor/{print $3}' /proc/cpuinfo | wc -l`
sanitizacion='^[0-9]+$'
# https://stackoverflow.com/a/806923
if ! [[ $nucleos =~ $sanitizacion ]] ; then
    nucleos=1
fi

echo se recomiendan $nucleos nucleos para la compilación
make -j$nucleos


#echo Limpiando temporales
#cd $HOME
#rm -r $TMP_DIR

echo
echo
echo
echo todo listo - instalar con:
echo "cd $TMP_DIR/Python-$PYTHON_VER && make install"
echo
#cd $TMP_DIR/Python-$PYTHON_VER
#make install
echo
echo Instale su Entorno Virtual utilizando el siguiente comando
echo
echo $DIR_INSTALL/bin/python3 -m venv directorio
