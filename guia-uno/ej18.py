# problema con esta solución:
# el enunciado del profesor solicita trabajar con palabras, y aquí se aceptan
# todos los caracteres entre cada espacio en blanco, que no concuerda con la
# definición de "palabra". se debería remover cada caracter no alfa, pero eso
# puede ser un conflicto con los signos diacríticos de varios idiomas.
#
# idealmente:
#     word.replace(/[^a-z]/i)
# ó para todos los idiomas:
#     word.replace(/[^:alpha:]/)
# si es que el engine de python lo soporta.

def main():
	print('Ingrese una frase:')
	first = input().split()

	print('Ingrese otra frase:')
	second = input().split()

	both = []
	uniq_first = []
	uniq_second = []

	# por cada palabra de la primera lista
	for word in first:
		# si está también en la segunda,
		if word in second:
			# guardarlo como que está en ambas
			both.append(word)
		# de lo contrario,
		else:
			# recordar que está sólo en la primera
			uniq_first.append(word)

	# por cada palabra de la segunda lista
	for word in second:
		# si no está en la primera, es único aquí
		if word not in first:
			uniq_second.append(word)

	print('\nLista de palabras que aparecen en las dos frases:')
	print(' '.join(both))

	print('\nLista de palabras que aparecen en la primera frase, pero no en la segunda:')
	print(' '.join(uniq_first))

	print('\nLista de palabras que aparecen en la segunda frase, pero no en la primera:')
	print(' '.join(uniq_second))


main()
