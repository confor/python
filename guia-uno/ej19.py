from functions import ask_int, raw_print, print_matrix

def main():
	N = ask_int('Tamaño deseado de la matriz')
	A = []

	# inicializar a cero, al parecer se pueden
	# multiplicar arrays
	for i in range(0, N):
		A.append([])
		A[i] = [0] * N

	for i in range(0, N):
		for j in range(0, N):
			A[i][j] = ask_int('Valor para ' + str(i) + ',' + str(j))

	print('Matriz dada:')
	print_matrix(A, N)

	A.reverse()
	print('Matriz con los renglones intercambiados:')
	print_matrix(A, N)

main()
