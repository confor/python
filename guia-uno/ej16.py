from functions import ask_int

def main():
	length = ask_int('¿Cuántos números quiere ingresar?')
	k = ask_int('¿Contra qué numero comparar?')

	nums = []
	menores = []
	mayores = []
	iguales = 0

	for i in range(0, length):
		x = ask_int('Ingrese número #' + str(i + 1))
		nums.append(x)

		if x > k:
			mayores.append(x)
		elif x < k:
			menores.append(x)
		else:
			iguales = iguales + 1

	print('Comparandolo con k = ' + str(k) + ',')
	if len(menores) <= 0:
		print('- de los números dados, no hay ninguno menor que k')
	else:
		print('- menores:', menores)

	if len(mayores) <= 0:
		print('- de los números dados, no hay ninguno mayor que k')
	else:
		print('- mayores:', mayores)

	if iguales == 0:
		print('y no hay ninguno igual a k.')
	elif iguales == 1:
		print('y hay uno igual a k.')
	else:
		print('y hay ' + str(iguales) + ' iguales a k.')


main()
