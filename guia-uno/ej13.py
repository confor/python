from functions import ask_int, raw_print

def main():
	print('Se dibujará un hexágono')
	N = ask_int('Lado')

	# parte superior del dibujo. se que escribí este código hace
	# mas o menos dos horas, pero ya ni idea que hace. esos números
	# funcionan por alguna magia mítica del universo.
	for i in range(0, N):
		raw_print(' ' * (N - i - 1))
		raw_print('*' * (N + 2*i))
		raw_print('\n')

	# parte inferior
	for i in reversed(range(0, N - 1)):
		raw_print(' ' * (N - i - 1))
		raw_print('*' * (N + 2*i))
		raw_print('\n')


main()
