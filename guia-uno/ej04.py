# aguante javascript:
#     let x = 10,
#         y = 2
#     ('*'.repeat(x)+'\n').repeat(y)

from functions import ask_int, raw_print

def main():
	print('Se dibujará un rectángulo.')
	width = ask_int('Ancho')
	height = ask_int('Altura')

	if width <= 0:
		print('El ancho es nulo')
		return

	if height <= 0:
		print('La altura es nula')
		return

	# otra opción:
	# print(('*' * width + '\n') * height)

	for x in range(0, height):
		for y in range(0, width):
			raw_print('*')

		raw_print('\n')


main()
