# tiendo a solucionar un problema en javascript y luego portarlo, porque
# no me siento cómodo con python. aquí está el programa en js (es6):
#     const N = 4
#     f = x => -1 * Math.pow(-1, x) * Math.pow(x, x)
#     Array.from(Array(N).keys()).map(n => f(++n)).reduce((a, b) => a+b, 0)

from functions import ask_int

def f(x):
	y = x ** x

	# los pares son negativos
	if x % 2 == 0:
		return -y

	# de lo contrario, positivo
	return y


def main():
	print('Calcular: 1^1 - 2^2 + 3^3 - 4^4 + ... + n^n')

	n = ask_int('Ingrese un número')
	total = 0

	# x: [1..N]
	for x in range(1, n + 1):
		total = total + f(x)

	print('El resultado es', total)


main()
