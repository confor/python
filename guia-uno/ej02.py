# buscar año bisiesto, según calendario gregoriano
# se busca que:
# - p: n % 4 == 0
# - q: n % 100 == 0
# - r: n % 400 == 0
# - p && (~q || r)

# en javascript (node repl):
#     const N = 1400
#     (n => (n % 4==0) && ( !n % 100==0 || (n % 400==0)))(N)

from functions import ask_int

def main():
	year = ask_int('Ingrese un año')

	# (divisible por cuatro) y ([no divisible por 100] ó [divisible por 400])
	if (year % 4 == 0) and (not year % 100 == 0 or year % 400 == 0):
		print('Es un año bisiesto')
	else:
		print('No es bisiesto')


main()
