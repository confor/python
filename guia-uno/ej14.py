from functions import ask_int, raw_print

def main():
	print('Se dibujará un triángulo')
	h = ask_int('Altura')

	# dibuja la parte superior
	for i in range(0, h - 1):
		raw_print(' ' * (h - i - 1))
		raw_print('/')
		raw_print(' ' * (2 * i))
		raw_print('\\\n')

	# parte inferior
	raw_print('/')
	raw_print('_' * (2 * h - 2))
	raw_print('\\\n')


main()
