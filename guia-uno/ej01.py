from functions import ask_int

def main():
	now = -1

	# verificar que esté dentro de [0, 24)
	while now < 0 or now >= 24:
		# solicitar la hora
		now = ask_int('¿Qué hora es?')

	# solicitar las horas extras
	add = ask_int('¿Cuántas horas agregar al reloj?')
	then = now + add

	while then >= 24:
		then -= 24

	if then == 1:
		print('En ' + str(add) + ' horas, el reloj marcará la 1')
	else:
		print('En ' + str(add) + ' horas, el reloj marcará las ' + str(then))


main()
