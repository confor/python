def main():
	print('Ingrese un texto, se eliminarán palabras duplicadas:')
	text = input()

	text = text.split()
	print('El elemento es:', text)

	dedupe = []
	for word in text:
		if not word in dedupe:
			dedupe.append(word)

	print('Sin duplicados:', dedupe)


main()
