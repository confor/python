# TODO docstrings

# utilidad para pedir un número al usuario
def ask_int(msg):
	print(msg, end=': ')
	try:
		return int(input())

	# atrapar no-números
	except ValueError:
		print('\nSe esperaba un número :(')
		return ask_int(msg)

	# atrapar ^C y salir
	except KeyboardInterrupt:
		raise SystemExit


# imprimir texto sin newline al final
def raw_print(msg):
	return print(msg, end='', flush=True)


# imprimir matriz cuadrada
def print_matrix(matrix, N):
	for i in range(0, N):
		for j in range(0, N):
			raw_print(' ')
			# transformar número a texto, luego left-pad con 4 espacios
			raw_print(str(matrix[i][j]).rjust(4))

		raw_print('\n')
