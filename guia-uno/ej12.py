# javascript (es6, node repl):
#     const N = 5
#     for (let i = 1; i <= N; i++) console.log('*'.repeat(i))

from functions import ask_int

def main():
	print('Se dibujará un "triángulo"')
	N = ask_int('Altura')

	for i in range(1, N + 1):
		# multiplicar texto en python hace que se repita
		print('*'*i)


main()
