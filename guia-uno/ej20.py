from functions import print_matrix, ask_int

def main():
	N = ask_int('Tamaño deseado de la matriz identidad')
	A = []

	for i in range(0, N):
		A.append([])
		A[i] = [0] * N

	for i in range(0, N):
		for j in range(0, N):
			if i == j:
				A[i][j] = 1

	print_matrix(A, N)


main()
