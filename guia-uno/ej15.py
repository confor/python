from math import floor
from functions import ask_int

def rev(num):
	reverso = 0

	while num > 0:
		resto = num % 10
		reverso = reverso * 10 + resto
		num = floor(num / 10)

	return reverso


def main():
	num = ask_int('un número pls')
	reverso = rev(num)
	print('El reverso es', reverso)

	if num == reverso:
		print('es palíndromo!')


main()
